/*
 * datatypes.h
 *
 *  Created on: 03/10/2012
 *      Author: wendell
 */

#ifndef DATATYPES_H_
#define DATATYPES_H_

#include <iostream>
#include <ctime>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <iterator>
#include <algorithm>
#include <cmath>
#include <cassert>

using namespace std;

namespace OPFT
{

template<class T>

/**
 * Data manipulating class for the OPF classifier.
 * It holds the data containers as a vector of samples
 * and some properties of the data as attributes.
 */
class DataSample
{
public:
	uint nSamples;
	uint nFeatures;
	uint nClasses;
	vector<string> nomeAttribs;
	vector<vector<T> > sampleVector;
	vector<int> labels;

	DataSample() :
			nSamples(0), nFeatures(0), nClasses(0)
	{

	}
	;

	DataSample(string filename)
	{
		ReadFile(filename.c_str());
	}
	;

	DataSample(uint Samples, uint Features, uint Classes) :
			nSamples(Samples), nFeatures(Features), nClasses(Classes)
	{
		vector<T> sample(Features);
		for (uint i = 0; i < Samples; ++i)
		{
			for (uint j = 0; j < Features; ++j)
			{
				sample[j] = 0.0f;
			}
			sampleVector.push_back(sample);
		}
	}
	DataSample(vector<vector<T> > *SampleVector, vector<int> *LabelsVector) :
			nSamples(0), nFeatures(0), nClasses(0)
	{
		InitializeFromValues(SampleVector, LabelsVector);
	}
	;

	~DataSample()
	{

	}
	;

	//	DataSample & operator=(DataSample other)
	//	{
	//
	//	}

	void InsertSample(vector<T> featureVector, int label)
	{
		if (nFeatures == 0 && nClasses == 0 && nSamples == 0)
		{
			if (featureVector.size() == 0)
			{
				perror("Feature vector is empty.\n");
				return;
			}
			nFeatures = featureVector.size();
		}
		else if (nFeatures != 0 && nClasses != 0 && nSamples != 0
				&& nFeatures != featureVector.size())
		{
			perror("Feature vector have wrong dimension.\n");
			return;
		}
		sampleVector.push_back(featureVector);
		labels.push_back(label);
		++nSamples;
		nClasses = this->GetClasses();
		return;
	}
	;

	//private:

	T GetFeature(int sample, int feature)
	{
		if (sampleVector.size() != 0)
			return sampleVector[sample][feature];
		else
			return (T) INFINITY;
	}
	;

	int ReadFile(string filename)
	{
		vector<T> data;
		std::ifstream theStream(filename.c_str());
		if (!theStream)
			std::cerr << filename.c_str();
		while (true)
		{
			std::string line;
			std::getline(theStream, line);
			if (line.empty())
				break;
			std::istringstream myStream(line);
			std::istream_iterator<T> begin(myStream), eof;
			std::vector<T> numbers(begin, eof);
			labels.push_back((int) numbers.back());
			numbers.pop_back();
			sampleVector.push_back(numbers);
			// process line however you need
			//		        std::copy(numbers.begin(), numbers.end(), std::ostream_iterator<T>(std::cout, " "));
			//		        std::cout << '\n';
		}
		nFeatures = sampleVector[0].size();
		nSamples = sampleVector.size();
		nClasses = GetClasses();
		return EXIT_SUCCESS;
	}
	;
/**
 * Initialize the data manipulating class after the passed arguments.
 * The samples and labels must be read to stl vectors.
 * Revised 18/02/14 Ok.
 * @param samplesVector The stl vector containing the samples. All samples
 * 						 must have the same dimensions.
 * @param labelsVector	The corresponding labels for each sample.
 * @return EXIT_SUCCESS in case of no errors in reading the data, EXIT_ERROR otherwise.
 */
	int InitializeFromValues(vector<vector<T> > samplesVector,
			vector<int> labelsVector)
	{
		Reset();
		if (samplesVector.size() == 0)
		{
			perror("Features vector is empty.\n");
			return EXIT_FAILURE;
		}
		if (labelsVector.size() == 0)
		{
			perror("Labels vector is empty.\n");
			return EXIT_FAILURE;
		}
		if (samplesVector.size() != labelsVector.size())
		{
			perror("Features and labels vectors have different sizes.\n");
			return EXIT_FAILURE;
		}
		// Temporary variable to check the number of features
		// of each data sample.
		uint sz = samplesVector[0].size();
		typename vector<vector<T> >::iterator it;
		for (it = samplesVector.begin(); it != samplesVector.end(); ++it)
		{
			if (sz != it->size())
			{
				perror("Feature vectors' sizes not consistent.\n");
				return EXIT_FAILURE;
			}
		}
		nSamples = samplesVector.size();
		nFeatures = sz;
		labels.swap(labelsVector);
		sampleVector.swap(samplesVector);
		nClasses = this->GetClasses();

		return EXIT_SUCCESS;
	}
	;
	/**
	 * Cleans the data containers.
	 * Revised 18/02/14 OK.
	 */
	void Reset(void)
	{
		vector<vector<T> >().swap(sampleVector);
		vector<int>().swap(labels);
		nSamples = 0;
		nFeatures = 0;
		nClasses = this->GetClasses();
	}

	vector<T> GetSample(int sample)
	{
		return sampleVector[sample];
	}
	;

	int GetLabel(int sample)
	{
		if (!labels.empty())
			return labels[sample];
		else
			return -1;
	}
	;

	int GetClasses()
	{
		if (labels.size() == 0)
			return 0;
		set<int> classes;
		for (vector<int>::iterator it = labels.begin(); it != labels.end();
				++it)
		{
			classes.insert(*it);
		}
		return classes.size();
	}
	;

	int GetNFeatures(void)
	{
		return nFeatures;
	}
	;
};

typedef struct Coords2D_
{
	int X;
	int Y;
} Coords2D;

class lessCoords2D
{
public:
	lessCoords2D()
	{
	}
	;
	bool operator()(const Coords2D& a, const Coords2D&b) const
	{
		return sqrt(a.X * a.X + a.Y * a.Y) < sqrt(b.X * b.X + b.Y * b.Y);
	}
	;
};

typedef struct Edge_
{
	int head;
	int tail;
	float weight;

} Edge;

class lessEdge
{
public:
	lessEdge()
	{
	}
	;
	bool operator()(const Edge& lhs, const Edge&rhs) const
	{
		return (lhs.weight > rhs.weight);
	}
	;
};

struct lessMap
{
	template<typename T>
	bool operator()(const T& pLhs, const T& pRhs)
	{
		return pLhs.second < pRhs.second;
	}
};

typedef struct ClassifierData_
{
	int root;
	float weight;
	int label;
} ClassifierData;

class Timer
{
public:
	Timer()
	{
		clock_gettime(CLOCK_REALTIME, &beg_);
	}

	double elapsed()
	{
		clock_gettime(CLOCK_REALTIME, &end_);
		return end_.tv_sec - beg_.tv_sec
				+ (end_.tv_nsec - beg_.tv_nsec) / 1000000000.;
	}

	void reset()
	{
		clock_gettime(CLOCK_REALTIME, &beg_);
	}

private:
	timespec beg_, end_;
};
}
#endif /* DATATYPES_H_ */
