/*
 * OPFTraining.hpp
 *
 *  Created on: 05/10/2012
 *      Author: wendell
 */

#ifndef OPFTRAINING_HPP_
#define OPFTRAINING_HPP_

#include "datatypes.hpp"

namespace OPFT
{
template<typename T>
class OPFClassifier
{
	//private:

public:
	DataSample<T> trainingData;
	vector<vector<float> > adjacencyMatrix, MSTadj;
	vector<Edge> MST;
	vector<int> prototypes;
	vector<ClassifierData> classifier;
	vector<vector<int> > confusionMatrix;
	Timer tmr;

	OPFClassifier()
	{

	}
	;
	OPFClassifier(DataSample<T> input) :
			trainingData(input)
	{
		if(input.nSamples == 0)
		{
			perror("The input doesn't contain any data\n");
		}
		adjacencyMatrix.clear();
		//cout << "Mounting adjacency matrix..." << endl;
		tmr.reset();
		ExtractAdjacencyMatrix(trainingData);
//		cout << "Time spent in adjacency matrix: " << tmr.elapsed()
//				<< " seconds." << endl;
//		cout << "Training OPF..." << endl;
		tmr.reset();
		Fit();
//		cout << "Time spent on training: " << tmr.elapsed() << " seconds."
//				<< endl;
	}
	~OPFClassifier()
	{

	}
	;
	int ReadData(DataSample<T> input)
	{
		trainingData = input;
		if(input.nSamples == 0)
		{
			perror("The input doesn't contain any data.\n");
			return EXIT_FAILURE;
		}
		vector<vector<float> >().swap(adjacencyMatrix);
//		cout << "Mounting adjacency matrix..." << endl;
		tmr.reset();
		ExtractAdjacencyMatrix(trainingData);
//		cout << "Time spent in adjacency matrix: " << tmr.elapsed()
//				<< " seconds." << endl;
//		cout << "Training OPF..." << endl;
		tmr.reset();
		Fit();
//		cout << "Time spent on training: " << tmr.elapsed() << " seconds."
//				<< endl;
		return EXIT_SUCCESS;
	}
	;
	//private:
	float EuclideanDistance(vector<T> a, vector<T> b)
	{
		if (a.size() != b.size())
			return -1;
		int n = a.size();
		float res = 0;
		for (int i = 0; i < n; ++i)
		{
			res += (abs(a[i] - b[i])) * (abs(a[i] - b[i]));
		}
		return res; //sqrt(res);
	}
	;

	int Fit()
	{
		ExtractMST();
		GetPrototypes();
		DijkstraMax();
		return EXIT_SUCCESS;
	}

	vector<int> Predict(DataSample<T> samples)
	{
		tmr.reset();
		float dist, w, cost;
//		float acost;
		int lbl;
		uint i, j;
//		uint s, t, u, v;
		vector<int> classified(samples.nSamples, -1);
		for (i = 0; i < samples.nSamples; ++i)
		{
			cost = INFINITY;
			lbl = -1;
			for (j = 0; j < trainingData.nSamples; ++j)
			{
				dist = EuclideanDistance(samples.sampleVector[i],
						trainingData.sampleVector[j]);
				w = max(dist, classifier[j].weight);
//				acost = cost;
//				t = *(reinterpret_cast<int *>(&cost));
				if (w < cost)
				{
					cost = w;
					lbl = classifier[j].label;
				}
//				s = *(reinterpret_cast<int *>(&classifier[j].weight));
//
//				u = *(reinterpret_cast<int *>(&dist));
//				v = *(reinterpret_cast<int *>(&w));
//				cout << "\nIter: " << i << "," << j << ":\n" << "cost: " << acost << ", " << hex << t << dec << "\nlbl: " << lbl <<
//					"\ndist: " << dist << ", " << hex << u << dec << "\ncjw: " << classifier[j].weight << ", " << hex << s << dec <<
//					"\nw: " << w << ", " << hex << v << dec << endl;
			}
			classified[i] = lbl;
		}
//		cout << endl;
//		cout << "Execution time (classification of " << samples.nSamples
//				<< " samples): " << tmr.elapsed() << " seconds. ";
//		cout << endl;
//		cout << endl;
		return classified;
	}
	;

	int ExtractAdjacencyMatrix(DataSample<T> data)
	{
		for (uint i = 0; i < data.nSamples; ++i)
		{
			vector<float> dist;
			for (uint j = 0; j < data.nSamples; ++j)
			{
				if (i == j)
					dist.push_back(INFINITY);
				else
					dist.push_back(
							EuclideanDistance(data.sampleVector[i],
									data.sampleVector[j]));
			}
			adjacencyMatrix.push_back(dist);
		}
		return EXIT_SUCCESS;
	}
	;

	void AdjacencyMatrix()
	{
		for (uint i = 0; i < adjacencyMatrix.size(); ++i)
		{
			for (uint j = 0; j < adjacencyMatrix.size(); ++j)
			{
				cout << adjacencyMatrix[i][j] << " ";
			}
			cout << endl;
		}
		cout << endl;
	}
	;

	int ExtractMST()
	{
		uint nv = 0;
		priority_queue<Edge, vector<Edge>, lessEdge> pq;
		priority_queue<Edge, vector<Edge>, lessEdge> pqc;
		//		cout << "adjacencyMatrix.size(): " << adjacencyMatrix.size() << endl;
		for (uint i = 0; i < adjacencyMatrix.size(); ++i)
		{
			for (uint j = i + 1; j < adjacencyMatrix.size(); ++j)
			{
				if (adjacencyMatrix[i][j] < INFINITY)
				{
					Edge el =
					{ i, j, adjacencyMatrix[i][j] };
					//					cout << el.head<< "," << el.tail << ": " << el.weight << endl;
					pq.push(el);
				}
			}
		}
		pqc = pq;
		//		cout << "size pq: " << pq.size() << endl;
		//		cout << pq.top().weight << " " << endl;
		int lim = pqc.size();
		for (int i = 0; i < lim; ++i)
		{
			//			cout << pqc.top().head << "-" << pqc.top().tail << ": "
			//					<< pqc.top().weight << endl;
			pqc.pop();
		}
		vector<int> labels;
		for (uint i = 0; i < adjacencyMatrix.size(); ++i)
		{
			labels.push_back(i);
		}
		int curLabel = 0;
		//		uint max_vert = labels.size();
		//		cout << "labels " << max_vert << endl;
		//		for (uint i = 0; i < labels.size(); ++i)
		//		{
		//			cout << labels[i] << " ";
		//		}
		//		cout << "MST Alg: " << endl;
		for (uint i = 0; i < adjacencyMatrix.size(); ++i)
		{
			vector<float> t(adjacencyMatrix.size(), INFINITY);
			MSTadj.push_back(t);
		}
		while (!pq.empty())
		{
			//			cout << "Rodada " << nv + 1 << endl << endl;
			Edge el = pq.top();
			pq.pop();
			if (labels[el.head] != labels[el.tail])
			{
				curLabel = labels[el.head];
				MST.push_back(el);
				MSTadj[el.head][el.tail] = el.weight;
				MSTadj[el.tail][el.head] = el.weight;
				nv++;
				for (uint i = 0; i < labels.size(); ++i)
				{
					if (labels[i] == curLabel)
					{
						//						cout << "Mudou labels[" << i << "] de " << labels[i]
						//								<< " para ";
						labels[i] = labels[el.tail];
						//						cout << labels[i] << endl;
					}
				}
				//				cout << endl;
			}

			//			cout << adjacencyMatrix.size() - 1 << endl;
			if (nv == adjacencyMatrix.size() - 1)
				break;
		}
		return EXIT_SUCCESS;
	}
	;

	int ExtractConfusionMatrix(vector<int> classified, vector<int> ground)
	{
		if(classified.size() != ground.size())
		{
			perror("Classified vector and ground truth vector have different sizes!\n");
			return EXIT_FAILURE;
		}
		for (uint i = 0; i < trainingData.nClasses; ++i)
		{
			vector<int> line(trainingData.nClasses, 0);
			confusionMatrix.push_back(line);
		}
		for (uint i = 0; i < classified.size(); ++i)
		{
			confusionMatrix[ground[i]][classified[i]]++;
		}
		return EXIT_SUCCESS;
	}
	;

	void PrintConfusionMatrix()
	{
		cout << endl << "Confusion Matrix: " << endl << endl;

		for (uint i = 0; i < confusionMatrix.size(); ++i)
		{
			for (uint j = 0; j < confusionMatrix.size(); ++j)
			{
				cout.width(5);
				cout << confusionMatrix[i][j] << " ";
			}
			cout << endl;
		}
	}
	;

	int GetPrototypes()
	{
		vector<int> protos;
		for (uint i = 0; i < MST.size(); ++i)
		{
			if (trainingData.labels[MST[i].head]
					!= trainingData.labels[MST[i].tail])
			{
				protos.push_back(MST[i].head);
				protos.push_back(MST[i].tail);
				//				cout << "Prot: " << MST[i].head << ", " << MST[i].tail << endl;
			}
		}
		prototypes.clear();
		prototypes = protos;
		return EXIT_SUCCESS;
	}
	;
	const vector<int> GetNeighborsMST(int vertice)
	{
		vector<int> vz;
		vector<float> line = MSTadj[vertice];
		vector<float>::iterator it;
		for (uint i = 0; i < line.size(); ++i)
		{
			if (line[i] != INFINITY)
				vz.push_back(i);
		}
		return vz;
	}
	;
	int DijkstraMax()
	{
		map<int, float> visited, weights;
		map<int, float>::iterator it;
		vector<int> roots(adjacencyMatrix.size(), -1);

		for (uint i = 0; i < prototypes.size(); ++i)
		{
			roots[prototypes[i]] = prototypes[i];
		}

		for (uint i = 0; i < adjacencyMatrix.size(); ++i)
		{
			visited[i] = INFINITY;
		}
		for (uint i = 0; i < prototypes.size(); ++i)
		{
			visited[prototypes[i]] = 0.0f;
		}
		weights = visited;
		//		for (map<int, float>::iterator i = visited.begin(); i != visited.end(); ++i)
		//		{
		//			cout << "Weights: " << i->first << "->" << i->second << endl;
		//		}
		//		for (uint i = 0; i < MST.size(); ++i)
		//		{
		//			cout << MST[i].head << "," << MST[i].tail << ":" << MST[i].weight
		//					<< endl;
		//		}
		//		for (uint i = 0; i < MSTadj.size(); ++i)
		//		{
		//			for (uint j = 0; j < MSTadj.size(); ++j)
		//			{
		//				cout.width(3);
		//				cout << right << MSTadj[i][j] << " ";
		//			}
		//			cout << endl;
		//		}
		//		cout << endl;
		while (!visited.empty())
		{
			vector<int> vizinhos;
			it = min_element(visited.begin(), visited.end(), lessMap());
			vizinhos = GetNeighborsMST(it->first);
			for (vector<int>::iterator i = vizinhos.begin();
					i != vizinhos.end(); ++i)
			{
				//				cout << it->first << " e " << *i << endl;
				map<int, float>::iterator iti = visited.find(*i);
				if (iti != visited.end())
				{
					float peso = max(weights[it->first],
							adjacencyMatrix[it->first][*i]);
					//					cout << "L: " << peso << ": " << weights[it->first] << " " << adjacencyMatrix[it->first][*i] << endl;
					if (peso < weights[*i])
					{
						weights[*i] = peso;
						visited[*i] = peso;
						roots[*i] = roots[it->first];
					}
				}
			}
			visited.erase(it);
		}
		for (uint i = 0; i < roots.size(); ++i)
		{
			ClassifierData el =
			{ roots[i], weights[i], trainingData.labels[roots[i]] };
			classifier.push_back(el);
		}
		//		cout << endl << endl;
		//		std::copy(roots.begin(), roots.end(), std::ostream_iterator<int>(
		//				std::cout, " "));
		//		cout << endl;
		//		map<int, float>::iterator itii;
		//		for (map<int, float>::iterator i = weights.begin(); i != weights.end(); ++i)
		//		{
		//			cout << i->second << " ";
		//		}
		//		cout << endl;
		//		std::copy(labels.begin(), labels.end(), std::ostream_iterator<int>(
		//						std::cout, " "));
		return EXIT_SUCCESS;
	}
	;
	void PrintAccuracy()
	{
		uint acertos = 0, total = 0, erros = 0;
		if (confusionMatrix.size() != 0)
		{
			for (register uint i = 0; i < confusionMatrix.size(); ++i)
			{
				for (register uint j = 0; j < confusionMatrix.size(); ++j)
				{
					if (i == j)
						acertos += confusionMatrix[i][j];
					else
						erros += confusionMatrix[i][j];
				}
			}
			total = erros + acertos;
		}
		cout << endl;
		cout << "Hits: " << acertos << endl;
		cout << "Misses: " << erros << endl;
		cout << "Accuracy: " << ((float) acertos / (float) total) * 100 << "% "
				<< endl;
	}
	;

	int GenerateClassifierMIFFile()
	{
		cout << "Size of Classifier: " << classifier.size() << endl;
		uint mempos = 0;

		if (!classifier.empty())
		{
			ofstream miffile;
			miffile.open("rom.mif");
			//			float f = 70.482f;
			//			int i = *(reinterpret_cast<int*>(&f));
			//			printf("%08x\n", i);
			miffile << "WIDTH=32;" << endl;
			miffile << "DEPTH=4096;" << endl << endl;
			miffile << "ADDRESS_RADIX=UNS;" << endl;
			miffile << "DATA_RADIX=HEX;" << endl << endl;
			miffile << "CONTENT BEGIN" << endl;
			miffile << dec << "\t" << mempos++ << "\t\t:\t";
			miffile << hex << trainingData.nSamples << ";";
			cout << endl;
			miffile << dec << "\t" << mempos++ << "\t\t:\t";
			miffile << hex << trainingData.nFeatures << ";";
			cout << endl;

			for (vector<ClassifierData>::iterator it = classifier.begin();
					it != classifier.end(); ++it)
			{
				ClassifierData n = *it;
				miffile << dec << "\t" << mempos++ << "\t\t:\t";
				int i = (int)*(reinterpret_cast<int*>(&n.weight));
				miffile << hex << i << ";" << endl;
				miffile << dec << "\t" << mempos++ << "\t\t:\t";
				i = *(reinterpret_cast<int*>(&n.label));
				miffile << hex << i << ";" << endl;
				miffile << dec << "\t" << mempos++ << "\t\t:\t";
				i = *(reinterpret_cast<int*>(&n.root));
				miffile << hex << i << ";" << endl;
			}
			for (uint i = 0; i < trainingData.nSamples; ++i)
			{
				for (uint j = 0; j < trainingData.nFeatures; ++j)
				{
					miffile << dec << "\t" << mempos++ << "\t\t:\t";
					int k =
							*(reinterpret_cast<int*>(&trainingData.sampleVector[i][j]));
					miffile << hex << k << ";" << endl;
//					miffile << trainingData.sampleVector[i][j] << ";" << endl;
				}
			}
			miffile << "\t[" << dec << mempos << ".."
					<< "4095]\t\t:\t00000000;\n";
			miffile << "END;";
			miffile.close();
		}
		else
			return EXIT_FAILURE;
		return EXIT_SUCCESS;
	}

	int GenerateTestingMIFFile(DataSample<T> data)
	{
		cout << "Size of Test Data: " << data.sampleVector.size() << " "
				<< data.nSamples;
		cout << endl;
		cout << "Number of Attributes: " << data.sampleVector[0].size();
		cout << endl;
		cout << "Number of data elements: "
				<< data.nSamples * data.sampleVector[0].size();
		cout << endl;
		uint mempos = 0;

		if (!data.sampleVector.empty())
		{
			ofstream miffile;
			miffile.open("ram.mif");
			//			float f = 70.482f;
			//			int i = *(reinterpret_cast<int*>(&f));
			//			printf("%08x\n", i);
			miffile << "WIDTH=32;" << endl;
			miffile << "DEPTH=16384;" << endl << endl;
			miffile << "ADDRESS_RADIX=UNS;" << endl;
			miffile << "DATA_RADIX=HEX;" << endl << endl;
			miffile << "CONTENT BEGIN" << endl;
			miffile << dec << "\t" << mempos++ << "\t\t:\t";
			miffile << hex << data.nSamples << ";";
			cout << endl;

			//MUDE AQUI PARA FUNCIONAR COM O DATABASE IRIS.
			//USE k PARA LIMITAR O NÙMERO DE AMOSTRAS
			uint k = 510;
			k = data.nSamples * data.sampleVector[0].size() > 16384 ?
					510 : data.sampleVector.size();
			for (uint i = 0; i < k; ++i)
			{
				vector<T> smp = data.sampleVector[i];
				for (typename vector<T>::iterator iti = smp.begin();
						iti != smp.end(); ++iti)
				{
					miffile << dec << "\t" << mempos++ << "\t\t:\t";
					float n = *iti;
					int i = *(reinterpret_cast<int*>(&n));
					miffile << hex << i << ";" << endl;
//					miffile << n << ";" << endl;
				}
			}
			miffile << "\t[" << dec << mempos << "..16383]\t\t:\t00000000;\n";
			miffile << "END;";
			miffile.close();
		}
		else
			return EXIT_FAILURE;
		return EXIT_SUCCESS;
	}
};
}
;
#endif /* OPFTRAINING_HPP_ */
