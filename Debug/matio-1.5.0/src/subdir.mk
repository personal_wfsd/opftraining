################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
O_SRCS += \
../matio-1.5.0/src/endian.o \
../matio-1.5.0/src/inflate.o \
../matio-1.5.0/src/io.o \
../matio-1.5.0/src/mat.o \
../matio-1.5.0/src/mat4.o \
../matio-1.5.0/src/mat5.o \
../matio-1.5.0/src/mat73.o \
../matio-1.5.0/src/matvar_cell.o \
../matio-1.5.0/src/matvar_struct.o \
../matio-1.5.0/src/read_data.o \
../matio-1.5.0/src/snprintf.o 

C_SRCS += \
../matio-1.5.0/src/._mat5.c \
../matio-1.5.0/src/._matvar_cell.c \
../matio-1.5.0/src/._matvar_struct.c \
../matio-1.5.0/src/endian.c \
../matio-1.5.0/src/inflate.c \
../matio-1.5.0/src/io.c \
../matio-1.5.0/src/mat.c \
../matio-1.5.0/src/mat4.c \
../matio-1.5.0/src/mat5.c \
../matio-1.5.0/src/mat73.c \
../matio-1.5.0/src/matvar_cell.c \
../matio-1.5.0/src/matvar_struct.c \
../matio-1.5.0/src/read_data.c \
../matio-1.5.0/src/snprintf.c 

OBJS += \
./matio-1.5.0/src/._mat5.o \
./matio-1.5.0/src/._matvar_cell.o \
./matio-1.5.0/src/._matvar_struct.o \
./matio-1.5.0/src/endian.o \
./matio-1.5.0/src/inflate.o \
./matio-1.5.0/src/io.o \
./matio-1.5.0/src/mat.o \
./matio-1.5.0/src/mat4.o \
./matio-1.5.0/src/mat5.o \
./matio-1.5.0/src/mat73.o \
./matio-1.5.0/src/matvar_cell.o \
./matio-1.5.0/src/matvar_struct.o \
./matio-1.5.0/src/read_data.o \
./matio-1.5.0/src/snprintf.o 

C_DEPS += \
./matio-1.5.0/src/._mat5.d \
./matio-1.5.0/src/._matvar_cell.d \
./matio-1.5.0/src/._matvar_struct.d \
./matio-1.5.0/src/endian.d \
./matio-1.5.0/src/inflate.d \
./matio-1.5.0/src/io.d \
./matio-1.5.0/src/mat.d \
./matio-1.5.0/src/mat4.d \
./matio-1.5.0/src/mat5.d \
./matio-1.5.0/src/mat73.d \
./matio-1.5.0/src/matvar_cell.d \
./matio-1.5.0/src/matvar_struct.d \
./matio-1.5.0/src/read_data.d \
./matio-1.5.0/src/snprintf.d 


# Each subdirectory must supply rules for building sources it contributes
matio-1.5.0/src/%.o: ../matio-1.5.0/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -I"/home/wendell/workspaceEEOPF/OPFTraining/include" -I/opt/MATLAB/extern/include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


