/*
 * main.cpp
 *
 *  Created on: 03/10/2012
 *      Author: wendell
 */

#include <iostream>
#include "OPFTraining.hpp"
#include "datatypes.hpp"

using namespace std;
using namespace OPFT;

int main(int argc, char** argv)
{
	OPFClassifier<float> OPF;
	DataSample<float> universe("resources/falhas.txt");
//	DataSample<float> universe("resources/falhas2.txt");
//	DataSample<float> universe("resources/iris.csv");
//	DataSample<float> universe("resources/iris2.csv");
	DataSample<float> training, testing;

	uint num = universe.sampleVector.size() * 0.50;
	set<int> t;
//	srand(time(0));
	while (t.size() != num)
	{
		int ind = (int) rand() / (float) RAND_MAX * universe.nSamples;

		t.insert(ind);
	}
	for (uint i = 0; i < universe.nSamples; ++i)
	{
		set<int>::iterator it = t.find(i);
		if (it != t.end())
		{
			training.sampleVector.push_back(universe.sampleVector[i]);
			training.labels.push_back(universe.labels[i]);
		}
		else
		{
			testing.sampleVector.push_back(universe.sampleVector[i]);
			testing.labels.push_back(universe.labels[i]);
		}
	}

	training.nFeatures = universe.nFeatures;
	training.nSamples = training.sampleVector.size();
	training.nClasses = training.GetClasses();

	testing.nFeatures = universe.nFeatures;
	testing.nSamples = testing.sampleVector.size();
	testing.nClasses = testing.GetClasses();

	OPF.ReadData(training);

	cout << "Número de amostras: " << universe.nSamples << endl;
	cout << "Conjunto de treinamento: " << training.nSamples << endl;
	cout << "Conjunto de teste: " << testing.nSamples << endl;

	cout << endl << "Classificador OPF: " << endl;

//	std::copy(testing.labels.begin(), testing.labels.end(), std::ostream_iterator<int>(std::cout, " "));

	vector<int> classified = OPF.Predict(testing);

//	std::copy(classified.begin(), classified.end(), std::ostream_iterator<int>(std::cout, " "));

	if(classified.size() > 510)
	{
		for (uint i = 0; i < 510; ++i) {
			cout << classified[i] << " ";
		}
	}
	else
	{
		for (uint i = 0; i < classified.size(); ++i) {
					cout << classified[i] << " ";
				}
	}
	cout << endl;

	cout << endl << "Resultados: " << endl;

	cout << classified.size() << " " << testing.labels.size() << endl;

	OPF.ExtractConfusionMatrix(classified, testing.labels);
	OPF.PrintConfusionMatrix();
	OPF.PrintAccuracy();

	OPF.GenerateClassifierMIFFile();
	OPF.GenerateTestingMIFFile(testing);

	cout << endl;

	int i = 0x40a33333;
	cout << i << endl;

	float a = *(reinterpret_cast<float*> (&i));

	cout << a << endl;

//	float a=5.0f, b=2.0f, c=0.0f;
//
//	c = a;
//	int i = *(reinterpret_cast<int*> (&c));
//	cout << "a:   " << hex << i << endl;
//	c = b;
//	i = *(reinterpret_cast<int*> (&c));
//	cout << "b:   "  << hex << i << endl;
//	c = a-b;
//	i = *(reinterpret_cast<int*> (&c));
//	cout << "a-b: "  << hex << i << endl;
//	c = (a-b)*(a-b);
//	i = *(reinterpret_cast<int*>(&c));
//	cout << "r^2: "  << hex << i << endl;
//	c = sqrt((a-b)*(a-b));
//	i = *(reinterpret_cast<int*>(&c));
//	cout << "dst: "  << hex << i << endl;

	return EXIT_SUCCESS;
}
